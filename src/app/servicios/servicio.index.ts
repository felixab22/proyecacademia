export { 
    AsistenciaRegistroService,
    AsistenciaListaService,
    CalendarioService,
    SucursalService 
    } from './asistencia/asistencia.service';

export { ProfesorService } from './profesor/profesor.service';
export { AlumnoService } from './alumno/alumno.service';
export { SidebarService } from './setting/sidebar.service';
